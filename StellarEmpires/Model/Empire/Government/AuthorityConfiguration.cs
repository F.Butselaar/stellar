﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StellarEmpires.Model.Empire.Government
{
    public class AuthorityConfiguration
    {
        public AuthorityType AuthorityType { get; }
        public AuthorityConfiguration(EthicsConfiguration ethicsConfiguration, AuthorityType authorityType)
        {
            switch (authorityType)
            {
                case AuthorityType.Democratic:
                    CompareEthics(new[] {EthicType.Authoritarian, EthicType.FanaticAuthoritarian},
                        ethicsConfiguration.EthicList);
                    break;
                case AuthorityType.Oligarchic:
                    CompareEthics(new[] {EthicType.FanaticAuthoritarian, EthicType.FanaticEgalitarian},
                        ethicsConfiguration.EthicList);
                    break;
                case AuthorityType.Dictatorial:
                    CompareEthics(new[] {EthicType.Egalitarian, EthicType.FanaticEgalitarian}, ethicsConfiguration.EthicList);
                    break;
                case AuthorityType.Imperial:
                    CompareEthics(new[] {EthicType.Egalitarian, EthicType.FanaticEgalitarian}, ethicsConfiguration.EthicList);
                    break;
            }

            AuthorityType = authorityType;
        }

        //TODO test
        public static void CompareEthics(EthicType[] blacklist, EthicType[] ethics)
        {
            foreach (var ethic in blacklist)
            {
                if (ethics.Contains(ethic))
                    throw new Exception("Blacklisted exception");
            }
        }
    }

    //TODO expand with the other available empires.
    public enum AuthorityType
    {
        Democratic,
        Oligarchic,
        Dictatorial,
        Imperial
    }
}