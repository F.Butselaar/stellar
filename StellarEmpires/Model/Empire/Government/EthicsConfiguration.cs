﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StellarEmpires.Model.Empire.Government
{
    public class EthicsConfiguration
    {
        public EthicType[] EthicList { get; }

        public EthicsConfiguration(params EthicType[] list)
        {
            //Throws exceptions when it runs into invalid ethics.
            ValidateEthics(list);
            EthicList = list;
        }

        /**
         * Converts an Ethics enum to it's point value.
         * @see Ethic
         */
        public static int EthicPointValue(EthicType ethicType)
        {
            var mod = (int) ethicType % 2;
            if (mod == -1)
                return 3;
            return mod + 1;
        }

        /**
         * Returns the fanatic or non fanatic variant of an ethic.
         */
        public static EthicType Similar(EthicType ethicType)
        {
            if (ethicType == EthicType.GestaltConsciousness)
                return EthicType.GestaltConsciousness;
            if ((int) ethicType % 2 == 1)
                return (EthicType) ((int) ethicType - 1);
            return (EthicType) ((int) ethicType + 1);
        }

        /**
         * Return the two ethics opposite of the one given.
         */
        public static IEnumerable<EthicType> Opposites(EthicType ethicType)
        {
            var similar = Similar(ethicType);
            var offset = (int) ethicType > 8 ? -8 : 8;

            return new[] {similar + offset, ethicType + offset};
        }

        /**
         * For each ethic, check that the rest of the ethics aren't invalid.
         * Inefficient, but for this small size it'll do.
         */
        private static void ValidateEthics(EthicType[] ethics)
        {
            var pointsSpent = 0;
            foreach (EthicType ethic in ethics)
                pointsSpent += EthicPointValue(ethic);
            if (pointsSpent != 3)
                throw new EthicsConfigurationException("Number of ethics points spent must be equal to 3");

            //If points are 3 and it contains this, we can return it.
            if (ethics.Contains(EthicType.GestaltConsciousness))
                return;
            foreach (var ethic in ethics)
            {
                var invalids = Opposites(ethic).Concat(new[] {Similar(ethic)});
                foreach (var invalid in invalids)
                {
                    if (ethics.Contains(invalid))
                        throw new EthicsConfigurationException("Selected ethics are clashing.");
                }
            }
        }
    }

    /**
    * Different possible ethics in Stellaris
    */
    public enum EthicType
    {
        GestaltConsciousness = -1,
        Militarist,
        FanaticMilitarist,
        Xenophobe,
        FanaticXenophobe,
        Egalitarian,
        FanaticEgalitarian,
        Materialist,
        FanaticMaterialist,
        Pacifist,
        FanaticPacifist,
        Xenophile,
        FanaticXenophile,
        Authoritarian,
        FanaticAuthoritarian,
        Spiritualist,
        FanaticSpiritualist,
    }
}