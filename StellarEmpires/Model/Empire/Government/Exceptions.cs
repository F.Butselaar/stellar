﻿using System;

namespace StellarEmpires.Model.Empire.Government
{
    public class EthicsConfigurationException : Exception
    {
        public EthicsConfigurationException()
        {
        }

        public EthicsConfigurationException(string message) : base(message)
        {
        }

        public EthicsConfigurationException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}