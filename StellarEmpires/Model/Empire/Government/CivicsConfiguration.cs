﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;

namespace StellarEmpires.Model.Empire.Government
{
    public class CivicsConfiguration
    {
        private Civic[] Civics;
        public CivicsConfiguration(EthicsConfiguration ethics, AuthorityConfiguration authority,
            params CivicType[] civics)
        {
            Civics = ConstructCivics(civics);
            
        }

        public static Civic[] ConstructCivics(CivicType[] civics)
        {
            var constructedCivics = new Civic[2];
            for (var i = 0; i < civics.Length; i++)
                constructedCivics[i] = CivicPool.GetInstance().GetCivic(civics[i]);
            return constructedCivics;
        }
    }

    /**
     * Intermediate step from JSON to Civic
     */
    public class CivicPoco
    {
        public string Name { get; set; }
        public string[] IncompatibleCivics { get; set; }
        public string[] IncompatibleEthics { get; set; }

        public string[] RequiredAuthorities { get; set; }
        public string[] RequiredEthics { get; set; }

        public CivicPoco()
        {
            IncompatibleCivics = new string[0];
            IncompatibleEthics = new string[0];
            RequiredAuthorities = new string[0];
            RequiredEthics = new string[0];
        }
        
        /**
         * Removes all spaces and prepares for Enum.parse().
         */
        public static string[] TrimStrings(params string[] list)
        {
            for (var i = 0; i < list.Length; i++)
                list[i] = list[i].Replace(" ", "");
            return list;
        }
    }

    public class Civic
    {
        public string Name { get;}
        public CivicType[] IncompatibleCivics { get; }
        public EthicType[] IncompatibleEthics { get; }

        public AuthorityType[] RequiredAuthorities { get; }
        public EthicType[] RequiredEthics { get;  }
        
        public Civic(CivicPoco poco)
        {
            Name = poco.Name.Replace(" ", "");
            IncompatibleCivics = new CivicType[poco.IncompatibleCivics.Length];
            IncompatibleEthics = new EthicType[poco.IncompatibleEthics.Length];
            RequiredAuthorities = new AuthorityType[poco.RequiredAuthorities.Length];
            RequiredEthics = new EthicType[poco.RequiredEthics.Length];
            poco.IncompatibleCivics = CivicPoco.TrimStrings(poco.IncompatibleCivics);
            poco.IncompatibleEthics = CivicPoco.TrimStrings(poco.IncompatibleEthics);
            poco.RequiredAuthorities = CivicPoco.TrimStrings(poco.RequiredAuthorities);
            poco.RequiredEthics = CivicPoco.TrimStrings(poco.RequiredEthics);
            for (var i = 0; i < poco.IncompatibleCivics.Length; i++)
                IncompatibleCivics[i] = (CivicType) Enum.Parse(typeof(CivicType), poco.IncompatibleCivics[i]);
            
            for (var i = 0; i < poco.IncompatibleEthics.Length; i++)
                IncompatibleEthics[i] = (EthicType) Enum.Parse(typeof(EthicType), poco.IncompatibleEthics[i]);
            
            for (var i = 0; i < poco.RequiredAuthorities.Length; i++)
                RequiredAuthorities[i] = (AuthorityType) Enum.Parse(typeof(AuthorityType), poco.RequiredAuthorities[i]);
            
            for (var i = 0; i < poco.RequiredEthics.Length; i++)
                RequiredEthics[i] = (EthicType) Enum.Parse(typeof(EthicType), poco.RequiredEthics[i]);
        }
    }

    public class CivicPool
    {
        public IDictionary<CivicType, Civic> Pool { get; }
        private const string Filename = "C:/Users/Floyd/RiderProjects/StellarEmpires/StellarEmpires/civics.json";
        private static CivicPool _instance;

        private CivicPool()
        {
            Pool = new Dictionary<CivicType, Civic>();
            var content = File.ReadAllText(Filename);
            var js = new JavaScriptSerializer();
            var civicPocos = js.Deserialize<CivicPoco[]>(content);
            var civics = new Civic[civicPocos.Length];
            for (var i = 0; i < civicPocos.Length; i++)
                civics[i] = new Civic(civicPocos[i]);
            foreach (var civic in civics)
            {
                Pool[(CivicType) Enum.Parse(typeof(CivicType), civic.Name, true)] = civic;
            }
        }

        public Civic GetCivic(CivicType civic)
        {
            return Pool[civic];
        }

        public Civic PocoToCivic(CivicPoco poco)
        {
            return new Civic(poco);
        }

        public static CivicPool GetInstance()
        {
            if (_instance == null)
                _instance = new CivicPool();
            return _instance;
        }
    }


    public enum CivicType
    {
        AgrarianIdyll,
        AristocraticElite,
        BeaconOfLiberty,
        CitizenService,
        CorveeSystem,
        CutthroatPolitics,
        DistinguishedAdmiralty,
        EfficientBureaucracy,
        Environmentalist,
        ExaltedPriesthood,
        FeudalSociety,
        FreeHaven,
        FunctionalArchitecture,
        IdealisticFoundation,
        ImperialCult,
        InwardPerfection,
        Meritocracy,
        MiningGuilds,
        NationalisticZeal,
        ParliamentarySystem,
        PhilosopherKing,
        PoliceState,
        ShadowCouncil,
        SlaverGuilds,
        WarriorCulture
    }
}