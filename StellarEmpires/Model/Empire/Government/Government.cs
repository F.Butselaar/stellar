﻿namespace StellarEmpires.Model.Empire.Government
{
    public class Government
    {
        public EthicsConfiguration Ethics { get; }
        public AuthorityConfiguration Authority { get; }
        public CivicsConfiguration Civics { get; }

        public Government(EthicType[] ethics, AuthorityType authorityType, CivicType[] civics)
        {
            Ethics = new EthicsConfiguration(ethics);
            Authority = new AuthorityConfiguration(Ethics, authorityType);
            Civics = new CivicsConfiguration(Ethics, Authority, civics);
        }
    }
}