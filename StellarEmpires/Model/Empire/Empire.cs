﻿using StellarEmpires.model;

namespace StellarEmpires.Model.Empire
{
    public class Empire
    {
        public string Name { get; }
        public Species.Species Species { get; }
        public StarSystem Class { get; }
        public Government.Government Government { get; }
    }


    
   
}