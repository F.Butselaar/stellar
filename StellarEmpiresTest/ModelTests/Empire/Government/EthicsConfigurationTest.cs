﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using StellarEmpires.Model.Empire.Government;

namespace StellarEmpiresTest.ModelTests.Empire.Government
{
    [TestFixture]
    public class EthicsConfigurationTest
    {
        private readonly EthicType[] _fanatics =
        {
            EthicType.FanaticAuthoritarian, EthicType.FanaticEgalitarian, EthicType.FanaticMaterialist,
            EthicType.FanaticMilitarist, EthicType.FanaticPacifist, EthicType.FanaticSpiritualist,
            EthicType.FanaticXenophile, EthicType.FanaticXenophobe
        };

        private readonly EthicType[] _nonFanatics =
        {
            EthicType.Authoritarian, EthicType.Egalitarian, EthicType.Materialist,
            EthicType.Militarist, EthicType.Pacifist, EthicType.Spiritualist,
            EthicType.Xenophile, EthicType.Xenophobe
        };

        private readonly EthicType _gestalt = EthicType.GestaltConsciousness;

        [Test]
        public void EthicPointValueTest()
        {
            Assert.AreEqual(EthicsConfiguration.EthicPointValue(_gestalt), 3);
            foreach (var fanatic in _fanatics)
                Assert.AreEqual(EthicsConfiguration.EthicPointValue(fanatic), 2);
            foreach (var nonFanatic in _nonFanatics)
                Assert.AreEqual(EthicsConfiguration.EthicPointValue(nonFanatic), 1);
        }

        [Test]
        public void SimilarTest()
        {
            Assert.AreEqual(EthicsConfiguration.Similar(_gestalt), EthicType.GestaltConsciousness);
            for(var i = 0; i < _fanatics.Length; i++)
                Assert.AreEqual(EthicsConfiguration.Similar(_fanatics[i]), _nonFanatics[i]);
            for(var i = 0; i < _nonFanatics.Length; i++)
                Assert.AreEqual(EthicsConfiguration.Similar(_nonFanatics[i]), _fanatics[i]);
        }

        [Test]
        public void OppositeTest()
        {
            EthicType ethicType = EthicType.Authoritarian;
            IEnumerable<EthicType> opposites = EthicsConfiguration.Opposites(ethicType);
            
        }

        [Test]
        public void EthicGoodWeatherTest()
        {
            new EthicsConfiguration(EthicType.Authoritarian, EthicType.Materialist, EthicType.Pacifist);
            new EthicsConfiguration(EthicType.Authoritarian, EthicType.FanaticPacifist);
        }
        [Test]
        public void EthicBadWeatherTest()
        {
            //Too many points
            Assert.Throws<EthicsConfigurationException>(() =>
            {
                new EthicsConfiguration(EthicType.GestaltConsciousness, EthicType.Authoritarian);
            });
            //Too few ethics
            Assert.Throws<EthicsConfigurationException>(() =>
            {
                new EthicsConfiguration(EthicType.Authoritarian);
            });
            //Incompatible ethics
            Assert.Throws<EthicsConfigurationException>(() =>
            {
                new EthicsConfiguration(EthicType.Authoritarian, EthicType.FanaticEgalitarian);
            });
        }
    }

    [TestFixture]
    public class EthicTypeTest
    {
        [Test]
        public void EthicTypeNumberingTest()
        {
            Assert.AreEqual((int)EthicType.GestaltConsciousness, -1);
            Assert.AreEqual((int) EthicType.FanaticEgalitarian, 5);
        }
    }
}