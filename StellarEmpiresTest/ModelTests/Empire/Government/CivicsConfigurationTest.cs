﻿using System;
using NUnit.Framework;
using StellarEmpires.Model.Empire.Government;

namespace StellarEmpiresTest.ModelTests.Empire.Government
{
    [TestFixture]
    public class CivicsConfigurationTest
    {
        private readonly CivicPool _pool = CivicPool.GetInstance();

        [Test]
        public void CivicPoolTest()
        {
            Assert.AreEqual(_pool.Pool.Count, 25);
            var enums = (CivicType[])Enum.GetValues(typeof(CivicType));
            foreach (CivicType civicType in enums)
            {
                Assert.NotNull(_pool.GetCivic(civicType));
            }
        }

        [Test]
        public void GetCivicTest()
        {
            var freeHaven = CivicType.FreeHaven;
            var civic = _pool.GetCivic(freeHaven);
            Assert.AreEqual("FreeHaven", civic.Name);
        }

        [Test]
        public void PocoToCivicTest()
        {
            var poco = new CivicPoco
            {
                Name = "FreeHaven",
            };
            var civic = _pool.PocoToCivic(poco);
            Assert.NotNull(civic);
            Assert.AreEqual(civic.Name, "FreeHaven");
        }

        [SetUp]
        
        [Test]
        public void CivicConfigurationTest()
        {
            
        }
        
        [Test]
        public void ConstructCivicsTest()
        {
            var civics = new []{CivicType.Environmentalist, CivicType.AgrarianIdyll};
            var constructed = CivicsConfiguration.ConstructCivics(civics);
            Assert.AreEqual(constructed[0].Name, "Environmentalist");
            Assert.AreEqual(constructed[1].Name, "AgrarianIdyll");

        }
    }
}